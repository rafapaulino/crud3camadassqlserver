﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Crud3CamadasSQLServer
{
    class BairroBLL
    {
        BairroDAL bairroDAL;

        public DataTable listaBairroDAL()
        {
            DataTable dataTable = new DataTable();
            try
            {
                bairroDAL = new BairroDAL();
                dataTable = bairroDAL.listaBairros(); 
            }
            catch(Exception erro)
            {
                throw erro;
            }
            return dataTable;
        }

        public void gravarDadosDAL(BairroModel bairro)
        {
            try
            {
                bairroDAL = new BairroDAL();
                bairroDAL.gravaBairro(bairro);
            }
            catch (Exception erro)
            {
                throw erro;
            }

        }

        public void excluirDadosDAL(BairroModel bairro)
        {
            try
            {
                bairroDAL = new BairroDAL();
                bairroDAL.excluirBairro(bairro);
            }
            catch (Exception erro)
            {
                throw erro;
            }

        }

        public void atualizaDadosDAL(BairroModel bairro)
        {
            try
            {
                bairroDAL = new BairroDAL();
                bairroDAL.atualizaBairro(bairro);
            }
            catch (Exception erro)
            {
                throw erro;
            }

        }

        public BairroModel pesquisaBairroDAL(String pesquisa)
        {
            BairroModel bairro = new BairroModel();
            try
            {
                bairroDAL = new BairroDAL();
                bairro = bairroDAL.pesquisaBairro(pesquisa);
            }
            catch (Exception erro)
            {
                throw erro;
            }
            return bairro;
        }

    }
}
