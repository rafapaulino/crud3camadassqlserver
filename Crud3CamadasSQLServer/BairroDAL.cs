﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Crud3CamadasSQLServer
{
    class BairroDAL
    {
        String conexaoSQLServer = @"Data Source=M002158\MSSQLSERVER01;Initial Catalog=banco_aula;Uid=rafa;pwd=123456;";
        SqlConnection conexao;

        public void desconectar()
        {
            conexao.Close();
        }

        public void conectar()
        {
            conexao = new SqlConnection(conexaoSQLServer);
        }

        public DataTable listaBairros()
        {
            try
            {
                this.conectar();
                SqlCommand sql = new SqlCommand("select * from bairro", conexao);
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = sql;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                return dataTable;
            }
            catch(Exception erro)
            {
                throw erro;
            }
            finally
            {
                this.desconectar();
            }
        }

        public void gravaBairro(BairroModel bairro)
        {
            try
            {
                this.conectar();
                SqlCommand sql = new SqlCommand("insert into bairro values (@bai_codigo, @bai_nome)", conexao);
                sql.Parameters.AddWithValue("@bai_codigo", bairro.Codigo);
                sql.Parameters.AddWithValue("@bai_nome", bairro.Nome);
                conexao.Open();
                sql.ExecuteNonQuery();
            }
            catch (Exception erro)
            {
                throw erro;
            }
            finally
            {
                this.desconectar();
            }
        }

        public void excluirBairro(BairroModel bairro)
        {
            try
            {
                this.conectar();
                SqlCommand sql = new SqlCommand("delete from bairro where bai_codigo = @codigo", conexao);
                sql.Parameters.AddWithValue("@codigo", bairro.Codigo);
                conexao.Open();
                sql.ExecuteNonQuery();
            }
            catch (Exception erro)
            {
                throw erro;
            }
            finally
            {
                this.desconectar();
            }
        }

        public void atualizaBairro(BairroModel bairro)
        {
            try
            {
                this.conectar();
                SqlCommand sql = new SqlCommand("update bairro set bai_nome = @nome where bai_codigo = @codigo", conexao);
                sql.Parameters.AddWithValue("@codigo", bairro.Codigo);
                sql.Parameters.AddWithValue("@nome", bairro.Nome);
                conexao.Open();
                sql.ExecuteNonQuery();
            }
            catch (Exception erro)
            {
                throw erro;
            }
            finally
            {
                this.desconectar();
            }
        }


        public BairroModel pesquisaBairro(string pesquisa)
        {
            try
            {
                this.conectar();
                SqlCommand sql = new SqlCommand("select * from bairro where bai_nome like '%" + pesquisa + "%'", conexao);
                //sql.Parameters.AddWithValue("@pesquisa", pesquisa);
                conexao.Open();
                SqlDataReader reader;
                BairroModel bairro = new BairroModel();
                reader = sql.ExecuteReader(CommandBehavior.CloseConnection);

                while(reader.Read())
                {
                    bairro.Codigo = Convert.ToInt32(reader["bai_codigo"]);
                    bairro.Nome = reader["bai_nome"].ToString();
                }
                return bairro;
            }
            catch (Exception erro)
            {
                throw erro;
            }
            finally
            {
                this.desconectar();
            }
        }
    }
}
