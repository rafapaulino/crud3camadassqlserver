﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crud3CamadasSQLServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.povoaGrade();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.povoaGrade();
        }

        public void povoaGrade()
        {
            try
            {
                BairroBLL bairroBLL = new BairroBLL();
                dataGridView1.DataSource = bairroBLL.listaBairroDAL();
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro na Listagem de Bairros:" + erro);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                BairroModel bairro = new BairroModel();
                bairro.Codigo = Convert.ToInt32(textBox2.Text);
                bairro.Nome = textBox3.Text;

                BairroBLL bairroBLL = new BairroBLL();
                bairroBLL.gravarDadosDAL(bairro);
                this.povoaGrade();
                MessageBox.Show("O Bairro foi adicionado com sucesso!");
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro na Gravação de Bairros:" + erro);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                BairroModel bairro = new BairroModel();
                bairro.Codigo = Convert.ToInt32(textBox2.Text);

                BairroBLL bairroBLL = new BairroBLL();
                bairroBLL.excluirDadosDAL(bairro);
                this.povoaGrade();
                MessageBox.Show("O Bairro foi Excluído com Sucesso!");
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro na Exclusão do Bairro:" + erro);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                BairroModel bairro = new BairroModel();
                bairro.Codigo = Convert.ToInt32(textBox2.Text);
                bairro.Nome = textBox3.Text;

                BairroBLL bairroBLL = new BairroBLL();
                bairroBLL.atualizaDadosDAL(bairro);
                this.povoaGrade();
                MessageBox.Show("O Bairro foi atualizado com sucesso!");
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro na Atualização do Bairro:" + erro);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                String pesquisa = textBox1.Text;
                BairroModel bairro = new BairroModel();

                BairroBLL bairroBLL = new BairroBLL();
                bairro = bairroBLL.pesquisaBairroDAL(pesquisa);

                textBox2.Text = bairro.Codigo.ToString();
                textBox3.Text = bairro.Nome;

                MessageBox.Show("O Bairro foi pesquisado com sucesso!");
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro na Pesquisa do Bairro:" + erro);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
            textBox3.Text = "";
        }
    }
}
